## Plexcol

Plexcol es un software para instituciones educativas de nivel superior, altamente eficaz y seguro.

# Requisitos

1. NodeJS v10.3.0 o superior
2. MySQL o MariaDB v10.1.26 o superior.
3. MySQL Workbench v6.3 o superior.

# ¿Como se utiliza?

1. Una vez instalado Node, clonar el repositorio (en una terminal: cd <Carpeta_donde_se_almacenara> && git clone https://bitbucket.org/altibajos/plexcol.git
2. Ya clonado, abrir una terminal y ejecutar "npm install" y "npm install nodemon".
3. Abrir el modelo de la base de datos ubicado en ["/Diagramas/Base de Datos.mwb"](https://bitbucket.org/altibajos/plexcol/src/master/Diagramas/Base%20de%20Datos.mwb) y realizar un forward engineer del modelo.
4. Ajustar la contraseña del servidor MySQL en ["/src/app.js"](https://bitbucket.org/altibajos/plexcol/src/master/src/app.js)
3. Una vez instaladas todas las dependencias, inicar el servidor cada vez que se requiera con el comando "npm run dev".

**El usuario inicial para ingresar al sistema tiene por nombre y contraseña 'admin'**
**Para ingresar al sistema, desde cualquier navegador, entrar a [localhost:3000](http://localhost:3000)**

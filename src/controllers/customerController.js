const controller = {};

controller.index = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select MONTHname(fecha) as mes,day(fecha) as dia,year(fecha) as año,descripcion, DATE_FORMAT(fecha, "%H:%i") as hora from calendario where curso_id IS NULL order by fecha asc', (err, resultado) => {
      console.log(resultado);
    if (err) {
     res.json(err);
    }
    res.render('index', {
       data: resultado
     });
    });
  });
};

controller.login = (req, res) => {
  req.getConnection((err, conn) => {
     res.render('login');
  });
};

controller.principal = (req, res) => {
  req.getConnection((err, conn) => {
     res.render('principal', {user: req.session.user,curso:req.session.curso,calendario:req.session.calendario});
  });
};

controller.logged = (req, res,next) => {
  const data = req.body;
  req.getConnection((err, connection) => {
    const query = connection.query("SELECT login('"+data.user+"','"+data.password+"') R;" , (err, resultado) => {
      if(resultado[0].R==1){
        req.session.user=data.user;
        console.log("user: "+data.user);
        req.getConnection((err, conn) => {
          conn.query('SELECT * FROM plexcol.asignatura WHERE usuario="'+data.user+'";', (err2, resultado2) => {
            console.log(resultado2);
            if (err2) {
             res.json(err2);
           }else{
             req.getConnection((err, conn) => {
               conn.query('SELECT * FROM plexcol.cursos WHERE user="'+data.user+'";', (err3, resultado3) => {
                 conn.query('select MONTHname(fecha) as mes,day(fecha) as dia,year(fecha) as año,descripcion, DATE_FORMAT(fecha, "%H:%i") as hora from calendario order by fecha asc;', (err4, resultado4) => {
                   console.log(resultado3);
                   req.session.user=resultado2;
                   if (err3 || err4) {
                     if (err3) {
                        res.json(err3);
                     }
                     if (err4) {
                       res.json(err4);
                     }
                   }else{
                     req.session.curso=resultado3;
                     req.session.calendario=resultado4;
                    res.render('principal', {user: req.session.user,curso:req.session.curso,calendario:req.session.calendario});
                  }
                  });
                });
               });
             /*res.render('principal', {user: resultado2});*/
           }
          });
        });
      }else{
        req.session.destroy(function(err){
          res.render('login', {fallido: true});
        });
      }
    });
  });
};

controller.destroySession = (req, res) => {
    req.session.destroy(function(err){
        res.redirect('/index');
    });
};

controller.edit = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM customer WHERE id = ?", [id], (err, rows) => {
      res.render('customers_edit', {
        data: rows[0]
      })
    });
  });
};

controller.update = (req, res) => {
  const { id } = req.params;
  const newCustomer = req.body;
  req.getConnection((err, conn) => {

  conn.query('UPDATE customer set ? where id = ?', [newCustomer, id], (err, rows) => {
    res.redirect('/');
  });
  });
};

controller.delete = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, connection) => {
    connection.query('DELETE FROM customer WHERE id = ?', [id], (err, rows) => {
      res.redirect('/');
    });
  });
}

module.exports = controller;

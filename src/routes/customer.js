const router = require('express').Router();

const customerController = require('../controllers/customerController');
const estudiantes = require('../controllers/controllerEstudiantes');

router.get('/', customerController.index);
router.get('/index', customerController.index);
router.get('/login', customerController.login);
router.post('/destroySession', customerController.destroySession);
router.post('/logged', customerController.logged);
router.post('/principal', customerController.principal);
//Estudiantes
router.post('/cursoEstudiante', estudiantes.cursoEstudiante);

module.exports = router;

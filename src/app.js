const express = require('express'),
      path = require('path'),
      morgan = require('morgan'),
      mysql = require('mysql'),
      cons = require('consolidate'),
      myConnection = require('express-myconnection'),
      session = require('express-session');

const app = express();

// importing routes
const customerRoutes = require('./routes/customer');

// settings
app.set('port', process.env.PORT || 3000);
app.engine('html', cons.swig);
//app.engine('php', php.Parser)
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'html');
app.set('view engine', 'ejs');

// middlewares
app.use(session({
    secret: 'plexcolUser',
    resave: true,
    saveUninitialized: true
}));
app.use(morgan('dev'));
app.use(myConnection(mysql, {
  host: 'localhost',
  user: 'root',
  password: 'nodarleanadie',
  port: 3306,
  database: 'plexcol'
}, 'single'));
app.use(express.urlencoded({extended: false}));

// routes
app.use('/', customerRoutes);

// static files
app.use(express.static("src/views"));
app.use(express.static(path.join(__dirname, 'public')));



// starting the server
app.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});
